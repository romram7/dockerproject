INSERT INTO public."Users"("User_Id","Name", "Password") VALUES (1, 'Marcel', 'PIGNOUL');
INSERT INTO public."Users"("User_Id","Name", "Password") VALUES (2, 'Jean', 'DUJARDIN');
INSERT INTO public."Users"("User_Id","Name", "Password") VALUES (3, 'John', 'DOE');
INSERT INTO public."Users"("User_Id","Name", "Password") VALUES (4, 'Jimmy', 'BROWN');
INSERT INTO public."Users"("User_Id","Name", "Password") VALUES (5, 'Fabriste', 'PIERRE');

INSERT INTO public."Basket"("Basket_Id", "User_Id", "Product_Id") VALUES (1, 1, 10);
INSERT INTO public."Basket"("Basket_Id", "User_Id", "Product_Id") VALUES (2, 2, 11);
INSERT INTO public."Basket"("Basket_Id", "User_Id", "Product_Id") VALUES (3, 3, 12);
INSERT INTO public."Basket"("Basket_Id", "User_Id", "Product_Id") VALUES (4, 4, 13);
INSERT INTO public."Basket"("Basket_Id", "User_Id", "Product_Id") VALUES (5, 5, 14);